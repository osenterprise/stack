-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: db
-- Tiempo de generaci�n: 20-02-2021 a las 19:46:02
-- Versi�n del servidor: 5.7.33
-- Versi�n de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `company`
--
CREATE DATABASE IF NOT EXISTS `company` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `company`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `lastname` varchar(70) NOT NULL,
  `function` varchar(70) NOT NULL,
  `email` varchar(70) NOT NULL,
  `city` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `name`, `lastname`, `function`, `email`, `city`) VALUES
(4, 'diego', 'purca', 'consultor TI', 'diego.purca@osenterprise.com.pe', 'Lima'),
(5, 'Marvin', 'Gonzales', 'gestor de proyectos', 'marvin.gonzales@osenterprise.com.pe', 'Lima'),
(6, 'Jorge', 'Arteaga', 'consultor TI', 'jorge.arteaga@osenterprise.com.pe', 'Lima'),
(7, 'Jorge', 'Bustamante', 'arquitecto', 'jorge@hotmail1.com', 'Lima'),
(8, 'Alberto', 'Alvan', 'recursos humanos', 'alvan@ti.com', 'Lima'),
(9, 'Gerardo', 'Garcia', 'abogado', 'garcia@talentoti.com', 'Arequipa'),
(10, 'Ursula', 'Vera', 'marketing', 'ursula@osenterprise.com', 'Iquitos');

--
-- �ndices para tablas volcadas
--

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Base de datos: `db1`
--
CREATE DATABASE IF NOT EXISTS `db1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db1`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;