// env global
def VERSION_APP = 'initial'
def ARTIFACT_ID = 'initial'
def GIT_URL='gitlab.com/osenterprise/petclinic.git'
def GIT_BRANCH='master'
def GIT_CRED='git-cred'
def MAVEN_SETTING='maven-petclinic-global'
def MAVEN_VERSION='maven-3.5.0'
def JAVA_VERSION='jdk-8'
pipeline {
    agent any
    stages {
        stage('Git Checkout') {
            steps {
                script {
                git branch: "$GIT_BRANCH", changelog: false, poll: false, url: "https://$GIT_URL"
                def mavenPom = readMavenPom file: 'pom.xml'
                VERSION_APP = "${mavenPom.version}"
                ARTIFACT_ID = "${mavenPom.artifactId}"
                }  
            }
        }
        stage('Unit Test') {
            steps {
                withMaven(globalMavenSettingsConfig: "$MAVEN_SETTING", jdk: "$JAVA_VERSION", maven: "$MAVEN_VERSION", mavenSettingsConfig: "$MAVEN_SETTING") 
                {
                sh 'mvn test'
                }
            }  
        }
        stage('Compile App Maven') {
            steps {
                withMaven(globalMavenSettingsConfig: "$MAVEN_SETTING", jdk: "$JAVA_VERSION", maven: "$MAVEN_VERSION", mavenSettingsConfig: "$MAVEN_SETTING") 
                {
                sh 'mvn package -Dmaven.test.skip'
                }
            }  
        }
    }
}