// env global
def VERSION_APP = 'initial'
def ARTIFACT_ID = 'initial'
def GIT_URL='gitlab.com/osenterprise/petclinic.git'
def GIT_BRANCH='master'
def GIT_CRED='git-cred'
def MAVEN_SETTING='maven-petclinic-global'
def MAVEN_VERSION='maven-3.5.0'
def JAVA_VERSION='jdk-8'
//env stage SAST Sonar
def SONAR_CRED='sonar-token'
def SONAR_SERVER='sonar-server'
def SONAR_INSTALL='sonar-4.8'
//env staget SAST Snyk
def SNYK_PROJECT='java-app-petclinic'
def SNYK_INSTALL='snyk-latest'
def SNYK_CRED='snyk-token'
pipeline {
    agent any
    stages {
        stage('Git Checkout') {
            steps {
                script {
                git branch: "$GIT_BRANCH", changelog: false, poll: false, url: "https://$GIT_URL"
                def mavenPom = readMavenPom file: 'pom.xml'
                VERSION_APP = "${mavenPom.version}"
                ARTIFACT_ID = "${mavenPom.artifactId}"
                }  
            }
        }
        stage('Unit Test') {
            steps {
                withMaven(globalMavenSettingsConfig: "$MAVEN_SETTING", jdk: "$JAVA_VERSION", maven: "$MAVEN_VERSION", mavenSettingsConfig: "$MAVEN_SETTING") 
                {
                sh 'mvn test'
                }
            }  
        }
        stage('Compile App Maven') {
            steps {
                withMaven(globalMavenSettingsConfig: "$MAVEN_SETTING", jdk: "$JAVA_VERSION", maven: "$MAVEN_VERSION", mavenSettingsConfig: "$MAVEN_SETTING") 
                {
                sh 'mvn package -Dmaven.test.skip'
                }
            }  
        }
        stage('SAST Test Sonarqube') {
            steps {
                script {
                def SCANNER_HOME = tool "$SONAR_INSTALL"
                withSonarQubeEnv(credentialsId: "$SONAR_CRED",  installationName: "$SONAR_SERVER") {
                sh "${SCANNER_HOME}/bin/sonar-scanner -Dsonar.java.binaries=** -Dsonar.language=java -Dsonar.java=binaries -Dsonar.sourceEnconding=UTF-8 -Dsonar.sources=src"
                }
            }
            }
        }
        stage('SAST Test Snyk') {
            steps {
                withMaven(globalMavenSettingsConfig: "$MAVEN_SETTING", jdk: "$JAVA_VERSION", maven: "$MAVEN_VERSION", mavenSettingsConfig: "$MAVEN_SETTING") 
                {
                snykSecurity failOnError: false, failOnIssues: false, projectName: "$SNYK_PROJECT", snykInstallation: "$SNYK_INSTALL", snykTokenId: "$SNYK_CRED"
                }
            }
        }
    }
}