// env global
def VERSION_APP = 'initial'
def ARTIFACT_ID = 'initial'
def GIT_URL='gitlab.com/osenterprise/petclinic.git'
def GIT_BRANCH='master'
def GIT_CRED='git-cred'
def MAVEN_SETTING='maven-petclinic-global'
def MAVEN_VERSION='maven-3.5.0'
def JAVA_VERSION='jdk-8'
//env stage SAST Sonar
def SONAR_CRED='sonar-token'
def SONAR_SERVER='sonar-server'
def SONAR_INSTALL='sonar-4.8'
//env staget SAST Snyk
def SNYK_PROJECT='java-app-petclinic'
def SNYK_INSTALL='snyk-latest'
def SNYK_CRED='snyk-token'
//env Staget build Image
def WAR_NAME='petclinic.war'
def PATH_DOCKERFILE='/var/jenkins_home/build-petclinic/'
def DOCKER_ACCOUNT='040500'
def DOCKER_REGISTRY='https://registry.hub.docker.com'
def DOCKER_CRED='dockerhub-credential'
def IMAGE_NAME='marzo-petclinic'
def ANCHORE_FILE='anchore_images'
//env Performance Test
def IP_WORKER='192.168.1.190'
def PORT_APP='32102'
//env OWASP ZAP
def DD_TOKEN='DD_Token'
def DD_IP='192.168.1.162'
def DD_PORT='8080'
def NUM_PROD='1'
def NUM_ENG='7'

pipeline {
    agent any
    stages {
        stage('Git Checkout') {
            steps {
                script {
                git branch: "$GIT_BRANCH", changelog: false, poll: false, url: "https://$GIT_URL"
                def mavenPom = readMavenPom file: 'pom.xml'
                VERSION_APP = "${mavenPom.version}"
                ARTIFACT_ID = "${mavenPom.artifactId}"
                }  
            }
        }
        stage('Unit Test') {
            steps {
                withMaven(globalMavenSettingsConfig: "$MAVEN_SETTING", jdk: "$JAVA_VERSION", maven: "$MAVEN_VERSION", mavenSettingsConfig: "$MAVEN_SETTING") 
                {
                sh 'mvn test'
                }
            }  
        }
        stage('Compile App Maven') {
            steps {
                withMaven(globalMavenSettingsConfig: "$MAVEN_SETTING", jdk: "$JAVA_VERSION", maven: "$MAVEN_VERSION", mavenSettingsConfig: "$MAVEN_SETTING") 
                {
                sh 'mvn package -Dmaven.test.skip'
                }
            }  
        }
        stage('SAST Test Sonarqube') {
            steps {
                script {
                def SCANNER_HOME = tool "$SONAR_INSTALL"
                withSonarQubeEnv(credentialsId: "$SONAR_CRED",  installationName: "$SONAR_SERVER") {
                sh "${SCANNER_HOME}/bin/sonar-scanner -Dsonar.java.binaries=** -Dsonar.language=java -Dsonar.java=binaries -Dsonar.sourceEnconding=UTF-8 -Dsonar.sources=src"
                }
            }
            }
        }
        stage('SAST Test Snyk') {
            steps {
                withMaven(globalMavenSettingsConfig: "$MAVEN_SETTING", jdk: "$JAVA_VERSION", maven: "$MAVEN_VERSION", mavenSettingsConfig: "$MAVEN_SETTING") 
                {
                snykSecurity failOnError: false, failOnIssues: false, projectName: "$SNYK_PROJECT", snykInstallation: "$SNYK_INSTALL", snykTokenId: "$SNYK_CRED"
                }
            }
        }
        stage('Build & Push Image') {
            steps {
                script {
                sh "cp /var/jenkins_home/workspace/$JOB_NAME/target/$WAR_NAME $PATH_DOCKERFILE"
                docker.withRegistry("$DOCKER_REGISTRY", "$DOCKER_CRED") 
                {
                def customImage = docker.build("$DOCKER_ACCOUNT/$IMAGE_NAME:$VERSION_APP", "$PATH_DOCKERFILE") 
                customImage.push()
                }
                }
            }
        }
        stage('Imagen Scane Anchore') {
            steps {
                sh "sed -i 's+${DOCKER_ACCOUNT}/${IMAGE_NAME}.*+${DOCKER_ACCOUNT}/${IMAGE_NAME}:${VERSION_APP}+g' $ANCHORE_FILE"
                anchore bailOnFail: false, bailOnPluginFail: false, name: "$ANCHORE_FILE"
            }
        }
        stage('ArgoCD Deploy App - Dev') {
            steps {
                withCredentials([usernamePassword(credentialsId: "$GIT_CRED", passwordVariable: 'GITPASSWORD', usernameVariable: 'GITUSERNAME')]) 
                {
                sh 'git config user.email miguel.vila@osenterprise.com.pe'
                sh 'git config user.name Jenkins'
                sh "git checkout $GIT_BRANCH"
                sh 'git pull origin master'
                sh "sed -i 's+${DOCKER_ACCOUNT}/${IMAGE_NAME}.*+${DOCKER_ACCOUNT}/${IMAGE_NAME}:${VERSION_APP}+g' deploy/dev/deployment.yaml"
                sh 'cat deploy/dev/deployment.yaml'
                sh 'git add deploy/dev/deployment.yaml'
                sh "git commit -m 'Update version: $VERSION_APP'"
                sh 'git push https://'+ GITUSERNAME +':'+ GITPASSWORD +"@$GIT_URL HEAD:$GIT_BRANCH"
                sh "sleep 120"
                }
            }
        }
        stage('ArgoCD Deploy App - Staging') {
            steps {
                withCredentials([usernamePassword(credentialsId: "$GIT_CRED", passwordVariable: 'GITPASSWORD', usernameVariable: 'GITUSERNAME')]) 
                {
                sh 'git config user.email miguel.vila@osenterprise.com.pe'
                sh 'git config user.name Jenkins'
                sh "git checkout $GIT_BRANCH"
                sh 'git pull origin master'
                sh "sed -i 's+${DOCKER_ACCOUNT}/${IMAGE_NAME}.*+${DOCKER_ACCOUNT}/${IMAGE_NAME}:${VERSION_APP}+g' deploy/staging/deployment.yaml"
                sh 'cat deploy/staging/deployment.yaml'
                sh 'git add deploy/staging/deployment.yaml'
                sh "git commit -m 'Update version: $VERSION_APP'"
                sh 'git push https://'+ GITUSERNAME +':'+ GITPASSWORD +"@$GIT_URL HEAD:$GIT_BRANCH"
                sh "sleep 120"
                }
            }
        }
        stage('Performance Test') {
            steps {
                sh "sed -i 's+IPSERVIDOR+$IP_WORKER+g' petclinic.jmx"
                sh "sed -i 's+PUERTOSERVIDOR+$PORT_APP+g' petclinic.jmx"
                sh "cat petclinic.jmx"
                sh '/var/jenkins_home/apache-jmeter/bin/jmeter.sh -JCookieManager.check.cookies=false -Jmeter.save.saveservice.output_format=xml -n -t petclinic.jmx -l Test.jtl'
                perfReport filterRegex: '', showTrendGraphs: true, sourceDataFiles: 'Test.jtl'
            }
        }
        stage('OWASP ZAP - DAST Test') {
            steps { 
                withCredentials([string(credentialsId: "$DD_TOKEN", variable: 'TOKEN')]) {
                sh 'docker run --rm --user root -v /opt/jenkins/jenkins_home/workspace/$JOB_NAME/:/zap/wrk/:rw owasp/zap2docker-stable zap-baseline.py -t http://'+ IP_WORKER+':'+ PORT_APP +'/petclinic -x testreport.xml || true'
                sh "curl --location --request POST http://"+ DD_IP +":"+ DD_PORT +"/api/v2/import-scan/ --header 'Authorization: Token "+ TOKEN +"' --form engagement=$NUM_ENG --form verified=true --form active=true --form 'scan_type=ZAP Scan' --form file=@testreport.xml"
                }
            }
        }
    }
}